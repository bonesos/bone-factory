#!/bin/bash

PACKAGES="busybox dropbear-bin udhcpc"
GIT_PACKAGES="cloudbits bones-init"

mkdir $1/rootfs -p
mkdir $1/debs
cd $1
build_dir=`pwd`

# create basic rootfs directories
cd $build_dir/rootfs
mkdir {var,lib,lib64,usr,bin,sbin,root,home,etc,opt,dev,proc,sys,media,mnt,tmp,run,srv}
mkdir var/run var/log usr/{bin,sbin,local,share,src,include,lib}


# Download and extract deb packages
cd $build_dir
cd debs
apt-deps $PACKAGES | xargs apt download
apt download $PACKAGES
cd $build_dir
for i in `ls debs`; do dpkg-deb -x debs/$i $build_dir/rootfs; done

# Create busybox links
cd $build_dir/rootfs/bin
for i in `./busybox --list-full`; do
  cd $build_dir/rootfs
  ln -s /bin/busybox $i
done

# clone bones-os repos
cd $build_dir
for i in $GIT_PACKAGES; do
  git clone https://gitlab.com/bonesos/$i
  cd $i/src
  cp -R * $build_dir/rootfs
  cd $build_dir
done

#tar up the image
cd $build_dir
tar -C rootfs -Jcvf $1.tar.xz .
